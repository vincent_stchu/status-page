import { GlLoadingIcon, GlAlert, GlBadge, GlTooltip } from '@gitlab/ui';
import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import axios from 'axios';
import * as timeago from 'timeago.js';
import { GlMarkdown, GlComment } from '~/components';
import { dateTimeMixin } from '~/utils/datetime';
import Details from '~/views/details.vue';

jest.mock('axios');
jest.mock('timeago.js');
jest.mock('~/utils/datetime');

const formattedDateTime = 'Feb 21, 2020 9:20am';
const formattedTimezone = 'GMT+2';
dateTimeMixin.methods.formatDateTime.mockReturnValue(formattedDateTime);
dateTimeMixin.methods.formatDateTimeZone.mockReturnValue(formattedTimezone);

axios.get.mockResolvedValue({
  data: {
    id: 1,
    title: 'Title',
    description: 'incident',
    status: 'closed',
    updated_at: '2020-02-21T07:05:38.261Z',
    comments: [],
  },
});

const formattedDate = '3 months ago';
timeago.format.mockReturnValue(formattedDate);

describe('Details page', () => {
  let wrapper;

  function mountComponent() {
    const $route = {
      params: {
        id: 1,
      },
    };
    wrapper = shallowMount(Details, {
      stubs: { RouterLink: RouterLinkStub },
      mocks: { $route },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findLoader = () => wrapper.findComponent(GlLoadingIcon);
  const findDescription = () => wrapper.findComponent(GlMarkdown);
  const findLinkBack = () => wrapper.findComponent(RouterLinkStub);
  const findAlert = () => wrapper.findComponent(GlAlert);
  const findBadge = () => wrapper.findComponent(GlBadge);
  const findComments = () => wrapper.findAllComponents(GlComment);
  const findTooltip = () => wrapper.findComponent(GlTooltip);

  it('renders the components when data received', () => {
    wrapper.setData({ loading: false });
    wrapper.vm.$nextTick(() => {
      expect(wrapper.element).toMatchSnapshot();
      expect(findDescription().exists()).toBe(true);
      expect(findLinkBack().exists()).toBe(true);
      expect(wrapper.element.textContent).toContain(`Edited ${formattedDate}`);
      expect(findTooltip().exists()).toBe(true);
      expect(findTooltip().text()).toContain(`${formattedDateTime} ${formattedTimezone}`);
    });
  });

  it('shows the loader while loading data', () => {
    wrapper.setData({ loading: true });
    wrapper.vm.$nextTick(() => {
      expect(findDescription().exists()).toBe(false);
      expect(findLoader().exists()).toBe(true);
      expect(findLinkBack().exists()).toBe(true);
    });
  });

  it('shows an alert when data load failed', () => {
    wrapper.setData({ loading: false, showAlert: true, incident: null });
    wrapper.vm.$nextTick(() => {
      expect(findDescription().exists()).toBe(false);
      expect(findLoader().exists()).toBe(false);
      expect(findLinkBack().exists()).toBe(true);
      expect(findAlert().exists()).toBe(true);
    });
  });

  it('renders the success badge based on an closed incident status', () => {
    expect(findBadge().exists()).toBe(true);
    expect(findBadge().attributes('variant')).toBe('success');
    expect(
      findBadge()
        .find('span')
        .classes(),
    ).toContain('text-capitalize');
  });

  it('renders the danger badge based on an open incident status', () => {
    wrapper.setData({ incident: { status: 'opened' } });
    expect(findBadge().exists()).toBe(true);
    expect(
      findBadge()
        .find('span')
        .classes(),
    ).toContain('text-capitalize');
    return wrapper.vm.$nextTick().then(() => {
      return expect(findBadge().attributes('variant')).toBe('danger');
    });
  });

  describe('Comments', () => {
    it('should not render comment block when there are no comments', () => {
      wrapper.setData({ incident: { comments: [] } });
      wrapper.vm.$nextTick(() => {
        expect(findComments().length).toBe(0);
      });
    });

    it('should  render comments', () => {
      wrapper.setData({
        incident: {
          comments: [
            { note: 'comment1', created_at: '13/10/2019' },
            { note: 'comment2', created_at: '13/11/2019' },
            { note: 'comment3', created_at: '13/01/2010' },
          ],
        },
      });
      wrapper.vm.$nextTick(() => {
        expect(findComments().length).toBe(3);
      });
    });
  });
});
