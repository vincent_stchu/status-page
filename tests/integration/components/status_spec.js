import { GlButton, GlIcon } from '@gitlab/ui';
import { shallowMount } from '@vue/test-utils';
import incidents from '../../mocks/incidents.json';
import { GlStatus } from '~/components';

const DEFAULT_ICON_CLASS = 'gl-icon s72';

describe('Status component', () => {
  let wrapper;

  function mountComponent({ props = { incidents } } = {}) {
    wrapper = shallowMount(GlStatus, {
      stubs: { GlIcon, GlButton },
      propsData: props,
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findIcon = () => wrapper.findComponent(GlIcon);
  const findContactButton = () => wrapper.findComponent(GlButton);
  const findStatusText = () => wrapper.find('[data-testid="incident-status-text"]');

  it('renders the status component with a icon and button', () => {
    const contactBtn = findContactButton();
    const icon = findIcon();
    expect(wrapper.element).toMatchSnapshot();
    expect(icon.exists()).toBe(true);
    expect(contactBtn.exists()).toBe(true);
    expect(contactBtn.text()).toBe('Contact support');
    expect(contactBtn.attributes('href')).toBe('https://support.gitlab.com/hc/en-us/requests/new');
  });

  it('renders success status when no open incidents are present', () => {
    const icon = findIcon();
    const status = findStatusText();
    expect(icon.props('name')).toBe('check-circle-filled');
    expect(icon.attributes('class')).toBe(`gl-text-green-500 ${DEFAULT_ICON_CLASS}`);
    expect(status.text()).toBe('All systems are operational!');
  });

  it('renders warning status when no open incidents are present', () => {
    const openIncident = { status: 'opened' };
    mountComponent({ props: { incidents: [...incidents, openIncident] } });
    const icon = findIcon();
    const status = findStatusText();
    expect(icon.props('name')).toBe('status_warning');
    expect(icon.attributes('class')).toBe(`gl-text-orange-300 ${DEFAULT_ICON_CLASS}`);
    expect(status.text()).toBe('Active incident');
  });
});
